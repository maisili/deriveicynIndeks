{
  description = "Deriveicyn Indeks";

  outputs = {
    self,

    niks,
    mksh

  }@argz:

  {
    datom = let
      inherit (builtins) removeAttrs;

    in removeAttrs argz [ "self" ];

  };
}
